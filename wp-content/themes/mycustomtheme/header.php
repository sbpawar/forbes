<?php 
DEFINE('TEMPDIR',get_template_directory_uri().'/');
$imgurl = TEMPDIR.'image/svgs/lock.svg';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php wp_head(); ?>
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet"/>

    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@700&display=swap" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;500&display=swap" rel="stylesheet"/>
    
    <!--<script src="https://code.jquery.com/jquery-3.5.1.js"></script>-->

    <title>Forbes - All Travel & Airlines Credit Cards</title>
    
    <script>
      var DOMAIN = "http://localhost/";
    </script>
  </head>
  <body>
  <div id="app">
    <header class="header-wrapper">
      <div class="header">
        <div class="menuicon">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
            fill="#fff"
            class="nav-open-close open w-5 h-6"
          >
            <path
              d="M2 5.5h16v2H2zM2 9.5h16v2H2zM2 13.5h16v2H2z"
              class="nav-open-close"
            ></path>
          </svg>
        </div>
        <a
          class="forbes-logo"
          href="#"
          aria-label="Forbes Logo"
          alt="Forbes Logo"
          ><svg
            class="fs-icon fs-icon--forbes-logo"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 200 54"
          >
            <path
              d="M113.3 18.2c0-5.8.1-11.2.4-16.2L98.4 4.9v1.4l1.5.2c1.1.1 1.8.5 2.2 1.1.4.7.7 1.7.9 3.2.2 2.9.4 9.5.3 19.9 0 10.3-.1 16.8-.3 19.3 5.5 1.2 9.8 1.7 13 1.7 6 0 10.7-1.7 14.1-5.2 3.4-3.4 5.2-8.2 5.2-14.1 0-4.7-1.3-8.6-3.9-11.7-2.6-3.1-5.9-4.6-9.8-4.6-2.6 0-5.3.7-8.3 2.1zm.3 30.8c-.2-3.2-.4-12.8-.4-28.5.9-.3 2.1-.5 3.6-.5 2.4 0 4.3 1.2 5.7 3.7 1.4 2.5 2.1 5.5 2.1 9.3 0 4.7-.8 8.5-2.4 11.7-1.6 3.1-3.6 4.7-6.1 4.7-.8-.2-1.6-.3-2.5-.4zM41 3H1v2l2.1.2c1.6.3 2.7.9 3.4 1.8.7 1 1.1 2.6 1.2 4.8.8 10.8.8 20.9 0 30.2-.2 2.2-.6 3.8-1.2 4.8-.7 1-1.8 1.6-3.4 1.8l-2.1.3v2h25.8v-2l-2.7-.2c-1.6-.2-2.7-.9-3.4-1.8-.7-1-1.1-2.6-1.2-4.8-.3-4-.5-8.6-.5-13.7l5.4.1c2.9.1 4.9 2.3 5.9 6.7h2V18.9h-2c-1 4.3-2.9 6.5-5.9 6.6l-5.4.1c0-9 .2-15.4.5-19.3h7.9c5.6 0 9.4 3.6 11.6 10.8l2.4-.7L41 3zm-4.7 30.8c0 5.2 1.5 9.5 4.4 12.9 2.9 3.4 7.2 5 12.6 5s9.8-1.7 13-5.2c3.2-3.4 4.7-7.7 4.7-12.9s-1.5-9.5-4.4-12.9c-2.9-3.4-7.2-5-12.6-5s-9.8 1.7-13 5.2c-3.2 3.4-4.7 7.7-4.7 12.9zm22.3-11.4c1.2 2.9 1.7 6.7 1.7 11.3 0 10.6-2.2 15.8-6.5 15.8-2.2 0-3.9-1.5-5.1-4.5-1.2-3-1.7-6.8-1.7-11.3C47 23.2 49.2 18 53.5 18c2.2-.1 3.9 1.4 5.1 4.4zm84.5 24.3c3.3 3.3 7.5 5 12.5 5 3.1 0 5.8-.6 8.2-1.9 2.4-1.2 4.3-2.7 5.6-4.5l-1-1.2c-2.2 1.7-4.7 2.5-7.6 2.5-4 0-7.1-1.3-9.2-4-2.2-2.7-3.2-6.1-3-10.5H170c0-4.8-1.2-8.7-3.7-11.8-2.5-3-6-4.5-10.5-4.5-5.6 0-9.9 1.8-13 5.3-3.1 3.5-4.6 7.8-4.6 12.9 0 5.2 1.6 9.4 4.9 12.7zm7.4-25.1c1.1-2.4 2.5-3.6 4.4-3.6 3 0 4.5 3.8 4.5 11.5l-10.6.2c.1-3 .6-5.7 1.7-8.1zm46.4-4c-2.7-1.2-6.1-1.9-10.2-1.9-4.2 0-7.5 1.1-10 3.2s-3.8 4.7-3.8 7.8c0 2.7.8 4.8 2.3 6.3 1.5 1.5 3.9 2.8 7 3.9 2.8 1 4.8 2 5.8 2.9 1 1 1.6 2.1 1.6 3.6 0 1.4-.5 2.7-1.6 3.7-1 1.1-2.4 1.6-4.2 1.6-4.4 0-7.7-3.2-10-9.6l-1.7.5.4 10c3.6 1.4 7.6 2.1 12 2.1 4.6 0 8.1-1 10.7-3.1 2.6-2 3.9-4.9 3.9-8.5 0-2.4-.6-4.4-1.9-5.9-1.3-1.5-3.4-2.8-6.4-4-3.3-1.2-5.6-2.3-6.8-3.3-1.2-1-1.8-2.2-1.8-3.7s.4-2.7 1.3-3.7 2-1.4 3.4-1.4c4 0 6.9 2.9 8.7 8.6l1.7-.5-.4-8.6zm-96.2-.9c-1.4-.7-2.9-1-4.6-1-1.7 0-3.4.7-5.3 2.1-1.9 1.4-3.3 3.3-4.4 5.9l.1-8-15.2 3v1.4l1.5.1c1.9.2 3 1.7 3.2 4.4.6 6.2.6 12.8 0 19.8-.2 2.7-1.3 4.1-3.2 4.4l-1.5.2v1.9h21.2V49l-2.7-.2c-1.9-.2-3-1.7-3.2-4.4-.6-5.8-.7-12-.2-18.4.6-1 1.9-1.6 3.9-1.8 2-.2 4.3.4 6.7 1.8l3.7-9.3z"
            ></path></svg
        ></a>

        <ul class="menu">
          <li class="show-1">Billionaires</li>
          <li class="show-1">Innovation</li>
          <li class="show-2">LeaderShip</li>
          <li class="show-2">Money</li>
          <li class="show-3">Consumer</li>
          <li class="show-3">Industry</li>
          <li class="show-3">Lifestyle</li>
          <li class="show-4">Featured</li>
          <li class="show-4">BrandVoice</li>
          <li class="show-4">Advisor</li>
        </ul>

        <?php 

          /*wp_nav_menu(
            array(
              //'theme_location' => 'top-menu',
              'menu' => 'Top Bar',
              'menu-class' => 'menu'
            )
          );*/
        ?>

        <div class="search-icon">
          <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQz4mDnqfUtsQ1KT-naZ-Utlltbty6w_d3vmA&usqp=CAU"
            height="auto"
            width="100%"
          />
        </div>
      </div>
    </header>
    <div class="category-wrap">
      <div class="category">
          CATEGORY
      </div>
      <div class="slider-container">
        <ul class="controls btnpos" id="customize-controls" aria-label="Carousel Navigation" tabindex="0">
          
        <button class="prev btnbg" data-controls="prev" aria-controls="customize" tabindex="-1">
                <i class="fas fa-angle-left fa-1x"></i>
            </button>
            <button class="next btnbg" data-controls="next" aria-controls="customize" tabindex="-1">
                <i class="fas fa-angle-right fa-1x"></i>
            </button>
        </ul>
      <ul class="category-list-wrap my-slider">
        <div class="slider-item">
          <li class="category-1">
              <div class="img-wrap"><img src="<?=$imgurl; ?>"></div>
              <div class="text">Rewards</div>
          </li>
          </div>
          <div class="slider-item">
          <li  class="category-2">
              <div class="img-wrap"><img src="<?=$imgurl; ?>"></div>
              <div class="text">Business</div>
          </li>
        </div>
        <div class="slider-item">
          <li class="category-3">
              <div class="img-wrap"><img src="<?=$imgurl; ?>"></div>
              <div class="text">Travel</div>
          </li>
          </div>
          <div class="slider-item">
          <li class="category-4">
              <div class="img-wrap"><img src="<?=$imgurl; ?>"></div>
              <div class="text">Entertainment</div>
          </li>
        </div>
        <div class="slider-item">
          <li class="category-5">
              <div class="img-wrap"><img src="<?=$imgurl; ?>"></div>
              <div class="text">Bad Credit</div>
          </li>
        </div>
        

      </ul>
    </div>
  </div>
