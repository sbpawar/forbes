<?php get_header(); 

$imgurl = TEMPDIR.'image/svgs/lock.svg';
?>

<div class="parent-container">  
    

    <section class="credit-travel-container-wrapper">
      <div class="credit-travel-container">
        
        
        <div class="credit-cards-travel">
          <div class="arrow-logo">
            <img src=<?php echo TEMPDIR."image/3.png"; ?> width="14px" height="10px" />
          </div>
          <span class="credit-span">Credit Cards</span>
          <span class="ccsep">|</span>
          <span class="travel-span">Travel & Airline</span>

        </div>
        <div class="ad-disclosure">
          Advertiser Disclosure
         </div>
        <!-- <div class="advertiser-disclosure">Advertiser Disclosure</div> -->
      </div>

      <div class="travel-airline-credit-wrapper">
        <div class="travel-airline-credit">
          <div class="logo-heading">
            <div class="flight-logo">
              <img src=<?php echo TEMPDIR."image/svgs/aeroplane.svg" ?> />
            </div>
            <div class="travel-tagline">All Travel &amp; Airlines Credit Cards</div>
          </div>
          <div class="travel-airline-credit-details">
            <p>
              Planning for a business or personal trip? Do not forget to pack the
              most important thing in your travel kit - A travel rewards credit
              card. Wherever your travels may bring you, your card is by your
              side. Count on us to help you find the card from our partners with
              the sweetest deals.
            </p>
          </div>
          <div class="summary">SUMMARY +</div>
        </div>
        <div class="aeroplane-img">
          <img src=<?php echo TEMPDIR."image/aeroplane.png" ?> width="267px" height="158px" />
        </div>
      </div>
    </section>
    <!--  -->

    <!-- features section start -->

    <section class="feature">
      <div>
        <div class="starmark"><img src=<?php echo TEMPDIR."assets/tagmark.png" ?> /></div>`
        <div class="feature-title">FEATURED</div>
        <div class="feature-heading">
          Business Platinum Card® from American Express
        </div>
      </div>
      <!-- <div> -->
      <div class="feature-details">
        <div class="feature-start">
          <div class="card-img">
            <img src=<?php echo TEMPDIR."assets/american-express.png" ?> />
          </div>
          <div class="card-btn"><button class="apply-btn btn">APPLY NOW</button></div>
          <div class="secure-msg">
            <p>On American Express Secure Website</p>
          </div>
          <div class="card-btn">
            <button class="compare-btn btn"><span>COMPARE</span></button>
          </div>
        </div>
        <div class="feature-middle">
          
          <div class="card-details">
            <p class="card-detail-1"
              >$150 statement credit after you spend $1,000 in purchases on your
              new Card within the first 3 months.</p>
              <p class="card-detail-2">
              3% cash back at U.S. supermarkets (on up to $6,000 per year in
              purchases, then 1%)</p>
            <p class="card-detail-3">2% cash back at U.S. gas stations and at select U.S. department
              stores, 1% back on other purchases.</p>
              <p class="card-detail-4">Low intro APR: 0% for 15 months on purchases and balance
              transfers, then a variable rate, currently 14.99% to 25.99%
            </p>
            <p class="card-detail-5">$150 statement credit after you spend $1,000 in purchases on your
              new</p>
          </div>
          <div class="more-info">MORE INFO</div>

          <div class="rating">
            <img
              src=<?php echo TEMPDIR."assets/svgs/combined-shape-copy-6.svg" ?>
              id="star-marked"
            /><img src=<?php echo TEMPDIR."assets/svgs/star-copy-10.svg" ?> id="star-unmarked" /><span
              >Read User Review (212)</span
            >
          </div>
        </div>
        <div class="feature-last">
          <div class="intro-r-1">
            <!-- <div> -->
            <div class="apr-title">INTRO APR</div>
            <div class="apr-percent">2.5%</div>
            <!-- </div> -->
            
            <span class="apr-logo  icon-tooltip icon-info tooltip tooltipstered" id="intr-apr"></span>
          </div>

          <div class="intro-apr">
            <!-- <div> -->
            <div class="apr-title">REGULAR APR</div>
            <div class="apr-percent">16.99-19.99%</div>
            <!-- </div> -->
            <span class="apr-logo icon-tooltip icon-info tooltip tooltipstered" id="reg-apr"></span>
          </div>

          <div class="intro-apr">
            <!-- <div> -->
            <div class="apr-title">ANNUAL FEES</div>
            <div class="apr-percent">$ 550</div>
            <!-- </div> -->
          </div>
          <div class="intro-apr">
            <!-- <div> -->
            <div class="apr-title">Credit Score</div>
            <div class="apr-percent">Excellent</div>
            <!-- </div> -->
          </div>
          <div class="travel-airplane">
            <div class="r-1"><img src=<?php echo TEMPDIR."image/svgs/aeroplane.svg" ?> /></div>
            <div class="r-2">Travel & Airplane</div>
          </div>
        </div>
      </div>
      <!-- </div> -->
    </section>

    <!-- features section end -->


    
    <!-- Editor's picks start -->

    <section>
      <div class="editor-picks">
        <div class="picks-heading">Credit Cards   |   <span >Forbes Editor’s Picks<span/></div>
        <div class="picks-info">
          <div class="picks-col-1">
            <div class="picks-col-1-row-1"><img src=<?php echo TEMPDIR."assets/large-img.png" ?>  /></div>
            <div class="picks-col-1-row-2">Personal finance advisor from those who know money best</div>
            <div class="picks-col-1-row-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
            <div class="picks-col-1-row-4-wrapper">
              <div>
    <div class="common-picks-flex">
      <div class="picks-inner-img"><img src=<?php echo TEMPDIR."assets/small-img-2.png" ?> /></div>
      <div class="picks-inner-info">Personal finance advisor from those who know money best</div>
    </div>
    <div class="common-picks-flex">
      <div class="picks-inner-img"><img src=<?php echo TEMPDIR."assets/small-img-1.png" ?> /></div>
      <div class="picks-inner-info">Personal finance advisor from those who know money best</div>
    </div>
  </div>
        <div>
    <div class="common-picks-flex">
      <div class="picks-inner-img"><img src=<?php echo TEMPDIR."assets/small-img-2.png" ?> /></div>
      <div class="picks-inner-info">Personal finance advisor from those who know money best</div>
    </div>
    <div class="common-picks-flex">
      <div class="picks-inner-img"><img src=<?php echo TEMPDIR."assets/small-img-1.png" ?> /></div>
      <div class="picks-inner-info">Personal finance advisor from those who know money best</div>
    </div>
  </div>
            </div>
          </div>
          <div class="picks-col-2">
		  <div class="picks-col-2-1">
    <div class="picks-col-2-img"><img src=<?php echo TEMPDIR."assets/medium-img.png" ?> /></div>
    <div class="picks-col-2-title">Forbes Lists</div>
    <div class="picks-col-2-heading">Personal finance advisor from those who know money best</div>
    <div class="picks-col-2-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
  </div>
  <div class="picks-col-2-2">
    <div class="picks-col-2-img"><img src=<?php echo TEMPDIR."assets/medium-img.png" ?> /></div>
    <div class="picks-col-2-title">Forbes Lists</div>
    <div class="picks-col-2-heading">Personal finance advisor from those who know money best</div>
    <div class="picks-col-2-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
  </div>
        </div>
        </div>
      </div>
    </section>

  <!-- Editor's picks end -->

    <!-- personal finance start 6img row -->

    <section>
      <div class="flex flex-wrap justify-between mt-4">
        <sixcards cls="pt-10 pb-5 cursor-pointer border-b-2" imgdt=<?php echo TEMPDIR."assets/medium-img-3.png" ?>></sixcards>
        <sixcards cls="pt-10 pb-5 cursor-pointer border-b-2" imgdt=<?php echo TEMPDIR."assets/medium-img-6.png" ?>></sixcards>
        <sixcards cls="pt-10 pb-5 cursor-pointer border-b-2" imgdt=<?php echo TEMPDIR."assets/medium-img-5.png" ?>></sixcards>
        <sixcards cls="pt-10 pb-5 cursor-pointer hidden md:block border-b-2" imgdt=<?php echo TEMPDIR."assets/medium-img-4.png" ?>></sixcards>  
        <sixcards cls="pt-10 pb-5 cursor-pointer hidden md:block border-b-2" imgdt=<?php echo TEMPDIR."assets/medium-img-3.png" ?>></sixcards>  
        <sixcards cls="pt-10 pb-5 cursor-pointer hidden md:block border-b-2" imgdt=<?php echo TEMPDIR."assets/medium-img-5.png" ?>></sixcards>  
    </section>

    <!-- personal finance end 6img row -->

    <!--All credit cards section Starts  -->

    <div class="all-credit-wrapper">
      <div class="all-credit">
        <div class="all-credit-heading">
          <div>All Credit Cards</div>
          <div class="filter-logo"><img src=<?php echo TEMPDIR."assets/svgs/filter-copy.svg" ?> /></div>
        </div>
        <div class="credit-menu">
          <div class="credit-menu-list item-1">APR ^</div>
          <div class="credit-menu-list item-2">REGULAR APR ^</div>
          <div class="credit-menu-list item-3">FEES ^</div>
          <div class="credit-menu-list item-4">CREDIT SCORE ^</div>
        </div>
        <div class="all-credit-filter">
          <div class="left-section">
            <div class="filter-cards">
              <div>Filter Cards</div>
              <div class="right-logo">Clear</div>
            </div>
            <div class="sort-by" :class="{'closed': sb}">
              <div class="heading" @click="toggleAccordion('sb')">
                <div>Sort by</div>
                <div class="list">{{ sb == '' ? '-' :  '+' }}</div>
              </div>
              
              <div class="sub-category-wrapper filter-body">
                <div class="sub-category">
                  <input type="checkbox"/>
                  <div>Sort by 1</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 2</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 3</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 4</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 5</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 6</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 7</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 8</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 9</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Sort by 10</div>
                </div>
        
              </div>
            </div>
            <div class="categories" :class="{'closed': ct}">
              <div class="heading" @click="toggleAccordion('ct')">
                <div>Categories</div>
                <div class="list">{{ ct == '' ? '-' :  '+' }}</div>
              </div>
              <div class="sub-category-wrapper filter-body">
                <div class="sub-category">
                  <input type="checkbox"/>
                  <div>Rewards</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>0% APR</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Travel</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Hotel</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Airline</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Entertainment</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Dining</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Bad credit</div>
                </div>
              <div class="sub-category last">+ 11 more</div>
              </div>
            </div>
            <div class="annual-fees" :class="{'closed': af}">
              <div class="heading" @click="toggleAccordion('af')">
                <div>Annual fees</div>
                <div class="list">{{ af == '' ? '-' :  '+' }}</div>
              </div>
              <div class="sub-category-wrapper filter-body">
                <div class="sub-category">
                  <input type="checkbox"/>
                  <div>$ 300</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>$ 350</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>$ 400</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>$ 500</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>$ 600</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>$ 700</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>$ 800</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>$ 900</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>$ 1000</div>
                </div>
              </div>
            </div>
            <div class="credit-score" :class="{'closed': cs}">
              <div class="heading" @click="toggleAccordion('cs')">
                <div>Credit Score</div>
                <div class="list">{{ cs == '' ? '-' :  '+' }}</div>
              </div>
              <div class="sub-category-wrapper filter-body">
                <div class="sub-category">
                  <input type="checkbox"/>
                  <div>500</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>600</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>650</div>
                </div>
                
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>700</div>
                </div>
                
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>750</div>
                </div>
                
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>800</div>
                </div>
                
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>850</div>
                </div>
              </div>
            </div>
            <div class="card-network" :class="{'closed': cn}">
              <div class="heading" @click="toggleAccordion('cn')">
                <div>Card Network</div>
                <div class="list">{{ cn == '' ? '-' :  '+' }}</div>
              </div>
              <div class="sub-category-wrapper filter-body">
                <div class="sub-category">
                  <input type="checkbox"/>
                  <div>Card Network 1</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Card Network 2</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Card Network 3</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Card Network 4</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Card Network 5</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Card Network 6</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Card Network 7</div>
                </div>
              </div>
            </div>
            <div class="financial-institution" :class="{'closed': fi}">
              <div class="heading accordion" @click="toggleAccordion('fi')">
                <div>Financial Institution</div>
                <div class="list">{{ fi == '' ? '-' :  '+'}}</div>
              </div>
              <div class="sub-category-wrapper filter-body">
                <div class="sub-category">
                  <input type="checkbox"/>
                  <div>Financial Institution 1</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Financial Institution 2</div>
                </div>
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Financial Institution 3</div>
                </div>
                
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Financial Institution 4</div>
                </div>
                
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Financial Institution 5</div>
                </div>
                
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Financial Institution 6</div>
                </div>
                
                <div class="sub-category">
                  <input type="checkbox" />
                  <div>Financial Institution 7</div>
                </div>
              </div>
            </div>
          </div>
          <div class="right-section">
            <div class ="data-container">
              <creditcardslist v-if="cardIndex <= cardsToShow && cardIndex <= carddata.length" v-for="cardIndex in cardsToShow" :carddataprop="carddata[cardIndex - 1]" :clickeddt="clicked"  v-bind:key="carddata[cardIndex - 1].post_id" @addtocompareclicked="addToCompare"></creditcardslist>
          </div>

          <div class="btndiv" v-if="cardsToShow < carddata.length">
            <button class="loadbtn" :class="{active: addloadclass}" id="btnLoadMore" @click="callCardsToShow">LOAD MORE</button>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!--All credit cards section Ends  -->
     <!-- CREDIT CARD SECTION START -->
     <div class="credit-card">
      <div class="credit-card-text-wrapper">
        <div class="credit-card-text">Credit Cards</div>
      </div>
      <div class="credit-card-details">
        <div class="credit-card-info">
          <div class="credit-card-info-1">
            Try Forbes Advisor for Credit Cards
          </div>
          <div class="credit-card-info-2">
            Compare Cards to find one that suits your financial priorities
          </div>
        </div>
        <div class="credit-card-option">
          <div class="credit-card-dropdown-1">
            <select class="option-1 option-common">
              <option>My credit score</option>
            </select>
          </div>
          <div class="credit-card-dropdown-2">
            <select class="option-2 option-common">
              <option>I care most about</option>
            </select>
          </div>
          <div class="know-more">
            <div class="know-more-btn">KNOW MORE</div>
          </div>
        </div>
        <div class="credit-card-img">
          <img src=<?php echo TEMPDIR."assets/visa-card.png" ?> alt="credit-card-image" />
        </div>
      </div>
     
    </div>
    <!-- CREDIT CARD SECTION START -->
  </div>
  <div>
        <div class="creditcards-outer left-0 fixed w-full bg-white justify-center" :class="{'bottom-0' : showbar}">
          <div class="creditcards-cont w-full xl:flex items-center justify-center">
            <div class="xl:flex overflow-y-scroll cards-wrap">
              <div v-for="item in comparecards">
                 <addcomparecard :id="item[0]" :name="item[1]" :imgurl="item[2]" @clicked="removeCard"></addcomparecard>
               </div>
           </div>  
           <div>
         <button class="compare-now cursor-pointer" :disabled="isDisabled" v-bind:class="{btndbcmpnw: compnwdsb}">Compare Now</button></div>
           </div>
         </div>
       </div>
      


<?php get_footer(); ?>