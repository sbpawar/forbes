
Vue.component('creditcardslist', {
    props:['carddataprop','clickeddt'],
    template: `<section class="feature-final"><div>
    <div class="feature-title">FEATURED</div>
    <div class="feature-heading">
      {{ this.carddataprop.post_title }}
    </div>
  </div>
  <div class="feature-details">
    <div class="feature-start">
      <div class="card-img">
        <img :src="this.carddataprop.image" />
      </div>
      <div class="card-btn"><button class="apply-btn btn">APPLY NOW</button></div>
      <div class="secure-msg">
        <p>On American Express Secure Website</p>
      </div>
      <div class="card-btn">
        <button class="compare-btn btn" id="sd1" v-bind:class="{btndb: contains(this.clickeddt, carddataprop.post_id)}" v-on:click="addToCompare(carddataprop.post_id,carddataprop.post_title,carddataprop.image)"><span>COMPARE</span></button>
      </div>
    </div>
    <div class="feature-middle">
      
      <div class="card-details">
        <p class="card-detail-1"
          >{{ this.carddataprop.card_detail_1 }}</p>
          <p class="card-detail-2">
          {{ this.carddataprop.card_detail_2 }}</p>
        <p class="card-detail-3">{{ this.carddataprop.card_detail_3 }}</p>
        <p class="card-detail-5">{{ this.carddataprop.card_detail_4 }}</p>
      </div>
      <div class="more-info">MORE INFO</div>

      <div class="rating">
        <img
          src="assets/svgs/combined-shape-copy-6.svg"
          id="star-marked"
        /><img src="assets/svgs/star-copy-10.svg" id="star-unmarked" /><span
          >Read User Review (212)</span
        >
      </div>
    </div>
    <div class="feature-last">
      <div class="intro-r-1">
        <div class="apr-title">INTRO APR</div>
        <div class="apr-percent">2.5%</div>
        <span class="apr-logo icon-tooltip icon-info tooltip tooltipstered" id="intr-apr2"></span>
      </div>

      <div class="intro-apr">
        <div class="apr-title">REGULAR APR</div>
        <div class="apr-percent">16.99-19.99%</div>
        <span class="apr-logo icon-tooltip icon-info tooltip tooltipstered" id="reg-apr2"></span>
      </div>

      <div class="intro-apr">
        <div class="apr-title">ANNUAL FEES</div>
        <div class="apr-percent">$ 550</div>
      </div>
      <div class="intro-apr">
        <div class="apr-title">Credit Score</div>
        <div class="apr-percent">Excellent</div>
      </div>
      <div class="travel-airplane">
        <div class="r-1"><img src="image/svgs/aeroplane.svg" /></div>
        <div class="r-2">Travel & Airplane</div>
      </div>
    </div>
  </div></section>`,
  methods: {
      
    addToCompare: function(id,title,imgdata) {
            
        /**
         * Pass data to parent
         */

          this.$emit('addtocompareclicked', id,title,imgdata)
      },
      contains: function(arr, item)
      {
        console.log('this.id----^^^^^^^',arr);
          /**
           * check whether arr contains value
           */

          return arr.indexOf(item) != -1;
      }      
}
});


/**
 * Component to display cards
 */

 Vue.component('addcomparecard', {
    props:['id','name','imgurl'],
    
    template: '<div class="creditcards-data py-20px xl:flex xl:border items-center xl:h-64px relative xl:w-332px hidden"><img :src="this.imgurl" alt="" class="h-56px"><h3 class="text-333333 font-euclidcircularb font-bold text-18px ml-10px capitalize">{{ this.name }}</h3> <svg  v-on:click="clicked(this.id)" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" width="20" class="xl:block xl:absolute cursor-pointer ml-auto"><path d="M0 0h24v24H0V0z" fill="none" opacity=".87"></path> <path d="M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm4.3 14.3c-.39.39-1.02.39-1.41 0L12 13.41 9.11 16.3c-.39.39-1.02.39-1.41 0-.39-.39-.39-1.02 0-1.41L10.59 12 7.7 9.11c-.39-.39-.39-1.02 0-1.41.39-.39 1.02-.39 1.41 0L12 10.59l2.89-2.89c.39-.39 1.02-.39 1.41 0 .39.39.39 1.02 0 1.41L13.41 12l2.89 2.89c.38.38.38 1.02 0 1.41z"></path></svg></div>',
    
    methods: {
        clicked: function(id) {
        console.log('this.id',this.id);    
        /**
         * Pass data to parent
         */

          this.$emit('clicked', this.id)
      }
    }
  });


/**
 * Component for editors pick section
 */

 Vue.component('editorspick', {
    
    template: `<div>
    <div class="common-picks-flex">
      <div class="picks-inner-img"><img src="assets/small-img-2.png" /></div>
      <div class="picks-inner-info">Personal finance advisor from those who know money best</div>
    </div>
    <div class="common-picks-flex">
      <div class="picks-inner-img"><img src="assets/small-img-1.png" /></div>
      <div class="picks-inner-info">Personal finance advisor from those who know money best</div>
    </div>
  </div>`
});


/**
 * Component for six cards
 */

 Vue.component('sixcards', {
    props:['cls','imgdt'],
    template: `<div :class="this.cls" v-on:click="postredirect">
    <div class="w-full h-auto">
      <img :src="this.imgdt" />
    </div>
    <div class="pt-4 text-base font-normal leading-6 tracking-normal max-w-xs font-sixcard">
      standard finance advisor from those who know money best
    </div>
    <div class="pt-4 text-xs font-normal leading-6 tracking-normal font-sixcard">
      <span class="cursor-default">By </span> <span class="author1" v-on:click.stop="authorredirect">User Name</span> <span class="cursor-default text-gray-700">6 min read</span>
    </div>
  </div>`,
  methods: {
    authorredirect: function() {
        window.location.href = DOMAIN+"responsive/samplepage/author.php";
  },
  postredirect: function() {
    window.location.href = DOMAIN+"responsive/samplepage/post.php";
  },
}
});

/**
 * Component for editor's picks right section
 */

 Vue.component('epright', {
    props:['cls'],
    template: `<div :class="this.cls">
    <div class="picks-col-2-img"><img src="assets/medium-img.png" /></div>
    <div class="picks-col-2-title">Forbes Lists</div>
    <div class="picks-col-2-heading">Personal finance advisor from those who know money best</div>
    <div class="picks-col-2-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
  </div>`
});


const app = new Vue({
el: '#app',
data: {
    sb: true,
    ct: true,
    af: true,
    cs: true,
    cn: true,
    fi: true,
    carddata: {},
    cardsToShow: 2,
    addloadclass: false,
    comparecards: [],
    comparecardscount: 0,
    clicked: [],
    clickeddisable: [1,2,3,4,5],
    unc:[],
    showbar: false,
    all: false,
    compnwdsb: true,
    isDisabled: true
},
watch: {
    comparecards: function() {

        /**
         * Disable card add and hide/show bottom bar
         */

        if(this.comparecards.length === 3)
        {

            this.unc = [...this.clicked];
            this.clicked = [];
            this.clicked = this.clickeddisable;
            this.showbar = true;
            this.all = true;
            
        }
        else if(this.comparecards.length < 3 && this.comparecards.length > 0)
        {
            this.showbar = true;
        }
        else
        {
            this.showbar = false;
        }

        this.compnwdsb = this.isDisabled = (this.comparecards.length > 1) ? false : true;

  }
},
methods: {
    toggleAccordion: function(id) {
        this[id] = !this[id];  
    },
    callCardsToShow: function() {
        this.addloadclass = !this.addloadclass;
        setTimeout(() => {
            this.cardsToShow = this.cardsToShow + 2;
            this.addloadclass = !this.addloadclass;
        }, 2000);
    },
    addToCompare: function(id,name,imgurl){

        /**
         * if card already exist then remove 
         */

        if(this.clicked.includes(id))
        {
            this.removeCard(id);
            return false;
        }

        /**
         * add new card to array
         */

        this.clicked.push(id);
        let card = [id,name,imgurl];
        this.comparecards.push(card);

    },
    removeCard (value) {

        /**
         * Remove card from array
         */

        for(let i = 0; i < this.comparecards.length; i++)
        {
            if(this.comparecards[i][0] == value)
            {
                this.comparecards.splice(i, 1);

                if(this.all)
                {
                    this.clicked = this.unc;
                    this.clicked.splice(i,1);
                }
                else
                {
                    this.clicked.splice(i,1);   
                }
            }
        }
      }      
},
created() {

        /**
         * API call to get json data
         */

        /*axios.get('http://localhost/vuecomponent/carddata.json').then(response => {
            this.carddata = response.data
          })*/

          this.carddata =[{"featured": "FEATURED",
  "post_id": 1,
    "post_title": "Chase Freedom Flex℠",
    "image": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/american-express.png",
    "rating_img_marked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/combined-shape-copy-6.svg",
    "rating_img_unmarked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/star-copy-10.svg",
    "reviews": "Read User Review (212)",
    "card_detail_1": "$550 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.",
    "card_detail_2":"10% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)",
    "card_detail_3":"20% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.",
    "card_detail_4":"$550 statement credit after you spend $1,000 in purchases on your new",
    "intro_apr":"3.5",
    "regular_apr":"19.99-21.99%",
    "annual_fees":"$ 550",
    "credit_score":"Excellent"
  },
    {"featured": "FEATURED",
      "post_id": 2,
    "post_title": "Chase Sapphire Preferred® Card",
    "image": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/american-express.png",
    "rating_img_marked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/combined-shape-copy-6.svg",
    "rating_img_unmarked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/star-copy-10.svg",
    "reviews": "Read User Review (212)",
    "card_detail_1": "$650 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.",
    "card_detail_2":"30% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)",
    "card_detail_3":"20% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.",
    "card_detail_4":"$650 statement credit after you spend $1,000 in purchases on your new",
    "intro_apr":"2.5",
    "regular_apr":"16.99-19.99%",
    "annual_fees":"$ 450",
    "credit_score":"Average"},
    {"featured": "FEATURED",
      "post_id": 3,
    "post_title": "American Express® Gold Card",
    "image": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/american-express.png",
    "rating_img_marked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/combined-shape-copy-6.svg",
    "rating_img_unmarked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/star-copy-10.svg",
    "reviews": "Read User Review (212)",
    "card_detail_1": "$750 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.",
    "card_detail_2":"35% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)",
    "card_detail_3":"26% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.",
    "card_detail_4":"$750 statement credit after you spend $1,000 in purchases on your new",
    "intro_apr":"2.5",
    "regular_apr":"25.99-27.99%",
    "annual_fees":"$ 650",
    "credit_score":"Excellent"},
    {"featured": "FEATURED",
      "post_id": 4,
    "post_title": "Business Platinum Card® from American Express",
    "image": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/american-express.png",
    "rating_img_marked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/combined-shape-copy-6.svg",
    "rating_img_unmarked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/star-copy-10.svg",
    "reviews": "Read User Review (212)",
    "card_detail_1": "$150 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.",
    "card_detail_2":"3% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)",
    "card_detail_3":"2% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.",
    "card_detail_4":"$150 statement credit after you spend $1,000 in purchases on your new",
    "intro_apr":"2.5",
    "regular_apr":"16.99-19.99%",
    "annual_fees":"$ 550",
    "credit_score":"Excellent"},
    {"featured": "FEATURED",
      "post_id": 5,
    "post_title": "U.S. Bank Visa® Platinum Card",
    "image": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/american-express.png",
    "rating_img_marked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/combined-shape-copy-6.svg",
    "rating_img_unmarked": "http://localhost/forbes/wp-content/themes/mycustomtheme/assets/svgs/star-copy-10.svg",
    "reviews": "Read User Review (212)",
    "card_detail_1": "$150 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.",
    "card_detail_2":"3% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)",
    "card_detail_3":"2% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.",
    "card_detail_4":"$150 statement credit after you spend $1,000 in purchases on your new",
    "intro_apr":"2.5",
    "regular_apr":"16.99-19.99%",
    "annual_fees":"$ 550",
    "credit_score":"Excellent"}]
  

      }

    });


