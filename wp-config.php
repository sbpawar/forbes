<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'forbes_demo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'bd+v4g^_b6VPy`{HfERBx^[Pxmedb$LsBVkuoh9N&+Q3[n3eI@jRj0V)X# ^Jvs4' );
define( 'SECURE_AUTH_KEY',  'j!2B{C=a=M]PokL7b71.VO<l}cFL`o/nhD[r3f?O>T.>jRK-/**a}/9Rcn&A]j4%' );
define( 'LOGGED_IN_KEY',    ',08 ]qP,f6Wbw]8E/j!^/,m(^1ohDXa*F*#t>KJ$`N^u)T4oyLuQNorOrNb2OS{Y' );
define( 'NONCE_KEY',        'tqBmNxk^EK:-9lptAP++v24!PMJ_EN`H18Y0;;sD- )J3A>yN}]z9>|.7>[!hEg1' );
define( 'AUTH_SALT',        'A_OpLV7t;OxC%*rR^]<r(Y:PT^yVQoSRr8#Je*sFptNbMgM{FyiWy(ECD<SID1f}' );
define( 'SECURE_AUTH_SALT', '-9![0.)v6ExyV(-K=rAw],Ky{G8k`BL:-x)As=>n$UZPBREl|_iXZfyG1d@>;GaZ' );
define( 'LOGGED_IN_SALT',   ',>LU y<!r2mefo3$4H -~ Ck~x-EDM.a5fVfVfF*WyO<Y.%tql WpR1W-im6JeG1' );
define( 'NONCE_SALT',       ',Kt-m#^ju9=1xl:{EyJ{!oD(Ni]my?Kxf@W,?,w]!|w0Icy[Z#Ign^;)KEG;4ZWK' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
