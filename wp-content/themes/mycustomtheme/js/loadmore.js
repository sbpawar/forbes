var datalength = i = 0;

get_append_data(); 

$("#btnLoadMore").on("click",loadMore); // attach click event to button for load more

/**
 * Show loader and remove hidden class from the section
 */

function loadMore(){
    
    $("#btnLoadMore").prop('disabled', true);
    $("#btnLoadMore").addClass("active");
    
    i++;

    setTimeout(function(){ 

        /**
         * Remove load more button when it is last item
         */

        if(i === datalength)
            $("#btnLoadMore").remove();

        $("#btnLoadMore").prop('disabled', false);
        $("#btnLoadMore").removeClass("active");
        $(".data-container .hidden").slice(0,1).removeClass("hidden");
    }, 2000);
}

/**
 * Fetching data from the json file and creating html 
 */

function get_append_data()
{
    // Make call to read json file
    $.getJSON("carddata.json", 
            function (data) {
        let appendhtml = '';
        datalength = data.length;

        // Traverse the data to create and append html
        $.each(data, function (key, value) {
            let featured = (value.featured) ? value.featured : '';
            let post_title = (value.post_title) ? value.post_title : '';
            let image = (value.image) ? value.image : '';
            let rating_marked = (value.rating_img_marked) ? value.rating_img_marked : '';
            let rating_unmarked = (value.rating_img_unmarked) ? value.rating_img_unmarked : '';
            let reviews = (value.reviews) ? value.reviews : '';
            let card_detail_1 = (value.card_detail_1) ? value.card_detail_1 : '';
            let card_detail_2 = (value.card_detail_2) ? value.card_detail_2 : '';
            let card_detail_3 = (value.card_detail_3) ? value.card_detail_3 : '';
            let card_detail_4 = (value.card_detail_4) ? value.card_detail_4 : '';
            let intro_apr = (value.intro_apr) ? value.intro_apr : '';
            let regular_apr = (value.regular_apr) ? value.regular_apr : '';
            let annual_fees = (value.annual_fees) ? value.annual_fees : '';
            let credit_score = (value.credit_score) ? value.credit_score : '';

            appendhtml += '<section class="feature-final hidden"><div><div class="feature-title">'+featured+'</div><div class="feature-heading">'+post_title+'</div></div><div class="feature-details"><div class="feature-start"><div class="card-img"><img src="http://localhost/forbes/wp-content/themes/mycustomtheme/'+image+'" /></div><div class="card-btn"><button class="apply-btn btn">APPLY NOW</button></div><div class="secure-msg"><p>On American Express Secure Website</p></div><div class="card-btn"><button class="compare-btn btn"><span>COMPARE</span></button></div></div><div class="feature-middle"><div class="card-details"><p class="card-detail-1">'+card_detail_1+'</p><p class="card-detail-2">'+card_detail_2+'</p><p class="card-detail-3">'+card_detail_3+'</p><p class="card-detail-5">'+card_detail_4+'</p></div><div class="more-info">MORE INFO</div><div class="rating"><img src="http://localhost/forbes/wp-content/themes/mycustomtheme/'+rating_marked+'" id="star-marked"/><img src="http://localhost/forbes/wp-content/themes/mycustomtheme/'+rating_unmarked+'" id="star-unmarked" /><span>'+reviews+'</span></div></div><div class="feature-last"><div class="intro-r-1"><div class="apr-title">INTRO APR</div><div class="apr-percent">'+intro_apr+'</div><div class="apr-logo">!</div></div><div class="intro-apr"><div class="apr-title">REGULAR APR</div><div class="apr-percent">'+regular_apr+'</div><div class="apr-logo">!</div></div><div class="intro-apr"><div class="apr-title">ANNUAL FEES</div><div class="apr-percent">'+annual_fees+'</div></div><div class="intro-apr"><div class="apr-title">Credit Score</div><div class="apr-percent">'+credit_score+'</div></div><div class="travel-airplane"><div class="r-1"><img src="http://localhost/forbes/wp-content/themes/mycustomtheme/image/svgs/aeroplane.svg" /></div><div class="r-2">Travel & Airplane</div></div></div></div></section>'; 
        });

        $('.data-container').append(appendhtml);
    });

}

/**
 * Accordin for filters
 */

filterAccordin();

function filterAccordin()
{
    let acc = document.getElementsByClassName("accordion");
    let j;

    for (j = 0; j < acc.length; j++) {
        acc[j].addEventListener("click", function() {
        let panel = this.nextElementSibling;
        
        /**
         * Apply animation when filter is clicked 
         */

        if($("."+this.id).hasClass('filtrans'))
        {
            var el = $('.'+this.id),
            curHeight = el.height(),
            autoHeight = el.css('height', 'auto').height();
            el.height(curHeight).animate({height: autoHeight}, 1);
            $("."+this.id).removeClass("filtrans");
            this.children[1].innerHTML = '-';
        }
        else{
            var el = $('.'+this.id),
            curHeight = el.height(),
            autoHeight = el.css('height', '0px').height();
            el.height(curHeight).animate({height: 0}, 1);
            $("."+this.id).addClass("filtrans");
            this.children[1].innerHTML = '+';
        }
        });
    }
} 

/**
 * redirecting to authors page
 * @param {object} e 
 */

function authorredirect(e) {
    e.stopPropagation();
    window.location.href = DOMAIN+"responsive/samplepage/author.php";
}

/**
 * redirecting to post page
 * @param {object} e 
 */

function postredirect(e) {
    //console.log('called: ',this.className);
    e.stopPropagation();
    window.location.href = DOMAIN+"responsive/samplepage/post.php";
}

/**
 * attaching click events on six cards
 */

[...document.querySelectorAll('.author1')].forEach(function(item) {
    item.addEventListener('click', authorredirect, false);
});

[...document.querySelectorAll('.personal-finance1')].forEach(function(item) {
    item.addEventListener('click', postredirect, false);
});

[...document.querySelectorAll('.personal-finance2')].forEach(function(item) {
    item.addEventListener('click', postredirect, false);
});

[...document.querySelectorAll('.personal-finance3')].forEach(function(item) {
    item.addEventListener('click', postredirect, false);
});

            
/*var author1 = document.getElementsByClassName('author1')[0];
var pf1 = document.getElementsByClassName('personal-finance1')[0];
var pf2 = document.getElementsByClassName('personal-finance2')[0];
var pf3 = document.getElementsByClassName('personal-finance3')[0];

author1.addEventListener('click', authorredirect, false);
pf1.addEventListener('click', postredirect, false);
pf2.addEventListener('click', postredirect, false);
pf3.addEventListener('click', postredirect, false);*/


