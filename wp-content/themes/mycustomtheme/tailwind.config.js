module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sixcard: ['Merriweather Sans'],
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
