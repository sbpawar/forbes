<?php

// Load CSS
function load_css()
{
    wp_register_style('customcss', get_template_directory_uri(). '/css/style.css', array(), false, 'all');
    wp_enqueue_style('customcss');

    wp_register_style('normalizecss', get_template_directory_uri(). '/css/normalize.css', array(), false, 'all');
    wp_enqueue_style('normalizecss');

    /*wp_register_style('tailwindcss', 'https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css', array(), false, 'all');
    wp_enqueue_style('tailwindcss');*/

    wp_register_style('tailwindcss', get_template_directory_uri(). '/css/twstyle.css', array(), false, 'all');
    wp_enqueue_style('tailwindcss');


    /*wp_register_style('tabscss', get_template_directory_uri(). '/css/skeletabs.css', array(), false, 'all');
    wp_enqueue_style('tabscss');
    
    wp_register_style('tinycss', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.3.11/tiny-slider.css', array(), false, 'all');
    wp_enqueue_style('tinycss');*/
}

add_action('wp_enqueue_scripts', 'load_css');

// Load JS
function load_js()
{
    /*wp_register_script('mainjquery', 'https://code.jquery.com/jquery-3.5.1.js', array(), false, 'all');
    wp_enqueue_script('mainjquery');

    wp_register_script('skelejs', get_template_directory_uri(). '/js/skeletabs.js', array(), false, 'all');
    wp_enqueue_script('skelejs');

    wp_register_script('tinysliderjs', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.3.11/min/tiny-slider.js', array(), false, 'all');
    wp_enqueue_script('tinysliderjs');

    wp_register_script('easytooltipjs', get_template_directory_uri(). '/js/easyTooltip.min.js', array(), false, 'all');
    wp_enqueue_script('easytooltipjs');

    wp_register_script('customjs', get_template_directory_uri(). '/js/loadmore.js', array(), false, 'all');
    wp_enqueue_script('customjs');*/

    wp_enqueue_script('vue', 'https://unpkg.com/vue', [], '');           
    wp_enqueue_script('vueapp', get_template_directory_uri(). '/js/vueassign.js', [], '', true);


}

add_action('wp_enqueue_scripts', 'load_js');


// Theme Options

add_theme_support('menus');
//add_theme_support('custom-logo');

// Menus

/*register_nav_menus(

    array(
        'top-menu' => 'Top Menu Location',
        'mobile-menu' => 'Mobile Menu Location',
    )
);*/

